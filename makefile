CXX = clang++
CXXFLAGS = -std=c++14 -Wall -Wextra -pedantic -Wconversion -g3 -march=native -O3 -I /usr/include/Eigen# -fsanitize=memory

SRC = src/
build_dir = build/

vpath %.cc $(SRC)
vpath %.h $(SRC)

all: main
	./build/a
	gnuplot graph
	gnuplot error

main: $(addprefix $(build_dir),main.o)
	$(CXX) $^ $(CXXFLAGS) -o $(build_dir)a

$(build_dir)%.o: %.cc %.h
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(build_dir)%.o: %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	$(RM) -rf $(build_dir)*.o $(build_dir)a
