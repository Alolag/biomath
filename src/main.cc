#include <cmath>
#include <cstdio>
#include <chrono>
#include <fstream>
#include <string>
#include <iostream>
#include <stdexcept>

#include <Eigen/Dense>

using dl = double;

constexpr dl PI = 3.1415926535897932384626433832795;

using std::cout;
using std::cin;
using std::endl;

Eigen::VectorXd linspace(dl begin, dl dh, ssize_t count) {
    Eigen::VectorXd res(count);
    for (ssize_t i = 0; i < count; ++i) {
        res(i) = begin + dh * i;
    }
    return res;
}

class Kernal {
    public:
    dl b = 1;
    dl d = 0;
    dl d1 = 1;
    virtual dl m(dl) const = 0;
    virtual dl w(dl) const = 0;
    virtual dl extent(dl) const = 0;
    virtual void get(std::istream&, std::ostream&) = 0;
    virtual std::string title() const = 0;
};

class ExponentKernal : public Kernal {
    public:
    dl A, B;
    dl Q(dl x) const {
        return (9. * A * x * x - 16. * 3. * A * abs(x) + 56. * A + 9. * B) / 27.;
    }
    dl R(dl x) const {
        return A * x * x + B;
    }
    dl m(dl x) const override {
        return exp(-2 * abs(x));
    }
    dl w(dl x) const override {
        return exp(-abs(x)) * Q(x) / (1. + exp(-abs(x)) * R(x));
    }
    dl extent(dl x) const override {
        return 1. + exp(-abs(x)) * R(x);
    }
    void get(std::istream& in, std::ostream &out) override {
        out << "A, B:" << endl;
        in >> A >> B;
    }
    std::string title() const override {
        std::string res;
        res += "Graph with Exponental Kernal. A = " + std::to_string(A);
        res += " B = " + std::to_string(B) + ".";
        return res;
    }
};

class RationalKernel : public Kernal{
    dl A, p;

    public:
    dl m(dl x) const override {
        return p / (PI * (x*x+p*p));
    }
    dl w(dl x) const override {
        return A / (x*x + 9*p*p);
    }
    void get(std::istream& in, std::ostream &out) override {
        out << "A, p:" << endl;
        in >> A >> p;
    }
    dl extent(dl x) const override {
        return 1. + 24./71./(x*x+1.) + 40./71./(x*x+4);
    }
    std::string title() const override{
        std::string res;
        res += "Graph with RationalKernel. A = " + std::to_string(A);
        res += ". p = " + std::to_string(p) + ".";
        return res;
    }
};

struct Answer {
    Eigen::VectorXd xi, f, f_ext;
    long time;
    std::string title;
    public:
    Answer(Eigen::VectorXd xi,
            Eigen::VectorXd f,
            Eigen::VectorXd f_ext,
            long time,
            std::string title) :
        xi(xi), f(f), f_ext(f_ext), time(time), title(title) {}
};

dl integrateSimpson(dl dh, const Eigen::VectorXd &f) {
        if (f.size() % 2 == 0) {
        throw std::invalid_argument("Size must be odd\n");
    }
    static Eigen::VectorXd v;
    if (v.size() != f.size()) {
        v.setOnes(f.size());
        for (ssize_t i = 1; i < f.size() - 1; ++i) {
            if (i % 2 == 1) {
                v(i) = 4;
            } else {
                v(i) = 2;
            }
        }
    }
    dl res = f.dot(v);
    res *= dh / 3;
    return res;
}

Answer solve(const Kernal& ker) {
    ssize_t Nx;
    dl beta;

    cout << "Points, from" << endl;
    cin >> Nx >> beta;

    dl alfa = -beta;
    dl dh = (beta - alfa) / (Nx - 1);
    auto xi = linspace(alfa, dh, Nx);
    std::string title = "From " + std::to_string(alfa) + " to " + std::to_string(beta) + ". " + std::to_string(Nx) + "points.";

    auto start = std::chrono::steady_clock::now();
    
    Eigen::VectorXd w(Nx), m(Nx);
    for (ssize_t i = 0; i < Nx; ++i) {
        w(i) = ker.w(xi(i));
        m(i) = ker.m(xi(i));
    }
    dl w_inter = integrateSimpson(dh, w);

    Eigen::MatrixXd K(Nx, Nx);
    for (ssize_t x = 0; x < Nx; ++x) {
        // Simpson
        for (ssize_t y = 0; y < Nx; ++y) {
            K(x, y) = dh / 3. * ker.b * ker.m(xi(x) - xi(y));
        }
        for (ssize_t i = 1; i < Nx - 1; ++i) {
            if (i % 2 == 1) {
                K(x, i) *= 4;
            } else {
                K(x, i) *= 2;
            }
        }
    }

    auto CalN = [&ker, w_inter, w, dh](const Eigen::VectorXd &Q) {
        dl N = ker.b - ker.d;
        N /= ker.d1 * (integrateSimpson(dh, Q.cwiseProduct(w)) + w_inter);
        return N;
    };

    Eigen::VectorXd c(Nx);
    dl N = CalN(c);
    dl diff;
    do {
        c = ker.b * m / N + ker.b * K * c + ker.b * c.Ones(Nx);
        for (ssize_t i = 0; i < Nx; ++i) {
            c(i) /= ker.b + ker.d1 * w(i);
            c(i) -= 1;
        }
        dl Nold = N;
        N = CalN(c);
        diff = N - Nold;
        cout << std::scientific << "\rdiff = " << abs(diff);
    } while (abs(diff) >= 1e-13);
    cout << endl;

    Eigen::VectorXd extent(Nx);
    for (ssize_t i = 0; i < Nx; ++i) {
        extent(i) = ker.extent(xi(i));
    }
    c += c.Ones(Nx);

    auto end = std::chrono::steady_clock::now();
    return Answer(std::move(xi), std::move(c), std::move(extent),
        std::chrono::duration_cast<std::chrono::seconds>(end - start).count(),
        title
        );
}

template<class Title>
void save(Answer& ans, const Title& title) {
    std::ofstream out("out");
    long Nx = ans.xi.size();

    out << "#x\ty_Extant\ty_My\tError" << endl;
    for (long i = 0; i < Nx; ++i) {
        out << ans.xi(i) << "\t" << (ans.f_ext(i)) <<  "\t";
        out  << ans.f(i)<< "\t"<< 100*std::abs(ans.f(i) - ans.f_ext(i))/ans.f_ext(i) << endl;
    }

    out.close();

    out.open("graph");
    out << "set term png size 1920, 1080" << endl;
    out << "set output \"graph.png\"" << endl;
    out << "set title\"" << title << " " << ans.title << " Doing took " << ans.time << "s.\"\n";
    out << "plot \"out\" using 1:3 w l title \"My\", \"out\" using 1:2 w l title \"Exact\"" << endl;
    out.close();
    out.open("error");
    out << "set term png size 1920, 1080" << endl;
    out << "set output \"error.png\"" << endl;
    out << "set title\"" << title << " " << ans.title << " Doing took " << ans.time << "s.\"\n";
    out << "plot \"out\" u 1:4 w l title \"error in %\"" << endl;
}

int main(void) {
    ExponentKernal ker;
    ker.get(cin, cout);
    auto res = solve(ker);
    save(res, ker.title());
}
